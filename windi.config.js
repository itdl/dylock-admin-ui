import { defineConfig } from 'windicss/helpers';
function range(size, startAt = 1) {
	return Array.from(Array(size).keys()).map((i) => i + startAt);
}
export default defineConfig({
	darkMode: 'class', //使用class 不使用自定义属性
	preflight: false, //禁用样式重置
	safelist: [range(100).map((i) => `gap-${i}`), range(100).map((i) => `mb-${i}`)],
	//扩展
	theme: {
		extend: {},
	},
	shortcuts: {
		'page-container': 'pl-[10px] pt-[10px]',
		modal: 'rounded-[4px]',
		'modal-footer': 'w-full flex justify-center items-center',
		'modal-footer-btn': 'flex justify-center items-center !w-[100px] !h-[36px] !rounded-[4px] !mr-[10px] !text-[14px]',
	},

	//插件
	plugins: [],
	//提取
	extract: {
		include: ['src/**/*.{vue,html,jsx,tsx}'],
		exclude: ['node_modules/**/*', '.git/**/*', 'nginx/*', '*.html', 'static/*'],
	},
});

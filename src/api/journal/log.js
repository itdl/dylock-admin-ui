/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-11-04 20:54:35
 * @LastEditors: pj
 * @LastEditTime: 2022-11-04 20:54:36
 */
import request from '@/utils/request';
import store from '@/store';

// 登录日志列表
export function listLoginLogs(query) {
	return request({
		url: '/sys/login/logs/list',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

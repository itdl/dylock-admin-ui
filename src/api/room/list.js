/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-19 16:44:24
 * @LastEditors: pj
 * @LastEditTime: 2022-10-22 16:53:08
 */
import request from '@/utils/request';
import store from '@/store';

// 房间列表
export function listbuildingsRoom(query) {
	return request({
		url: '/sys/buildings/rooms/list',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

// 房源树
export function treebuildingsRoom(query) {
	return request({
		url: '/sys/buildings/buildingTree',
		method: 'post',
		data: { ...query, projectId: store.getters.projectid },
	});
}

// 房源列表
export function listbuildingsfy(query) {
	return request({
		url: '/sys/buildings/list',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

// 新增房间
export function addbuildingsRoom(data) {
	return request({
		url: '/sys/buildings/generate',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 更新房间
export function updatebuildingsRoom(data) {
	return request({
		url: '/sys/buildings/modify',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 删除房间
export function delbuildingsRoom(data) {
	return request({
		url: '/sys/buildings/remove',
		method: 'post',
		data: data,
	});
}

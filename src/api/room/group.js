/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-19 16:44:17
 * @LastEditors: pj
 * @LastEditTime: 2022-10-31 11:47:29
 */
import request from '@/utils/request';
import store from '@/store';

// 房源分组列表
export function listbuildingsGs(query) {
	return request({
		url: '/sys/buildings/groups/list',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

// 房源分组获取分组房间
export function listbuildingsRoomGs(query) {
	return request({
		url: '/sys/buildings/groups/rooms',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

// 新增房源分组
export function addbuildingsGs(data) {
	return request({
		url: '/sys/buildings/groups/add',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 分组绑定房源
export function buildingsGsRoom(data) {
	return request({
		url: '/sys/buildings/groups/bind',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 更新房源分组
export function updatebuildingsGs(data) {
	return request({
		url: '/sys/buildings/groups/modify',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 删除房源分组
export function delbuildingsGs(params) {
	return request({
		url: '/sys/buildings/groups/remove',
		method: 'get',
		params,
	});
}

// 房源分组详情
// export function buildingsDetail (query) {
//     return request({
//         url: '/sys/buildings/detail',
//         method: 'get',
//         params: query
//     })
// }

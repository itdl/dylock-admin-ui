import request from '@/utils/request';

// 门锁列表
export function listlock(query) {
	return request({
		url: '/sys/lock/list',
		method: 'get',
		params: query,
	});
}

import request from '@/utils/request';
import store from '@/store';

// 充值列表
export function listRecharges(query) {
	return request({
		url: '/sys/sms/recharges/list',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

// 短信列表
export function listSms(query) {
	return request({
		url: '/sys/gateway/records/list',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

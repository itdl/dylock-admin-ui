/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-15 15:50:56
 * @LastEditors: pj
 * @LastEditTime: 2022-11-02 18:28:21
 */
import request from '@/utils/request';
import store from '@/store';

// 账号列表
export function listaccounts(query) {
	return request({
		url: '/sys/accounts/list',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

// 新增账号
export function addaccounts(data) {
	return request({
		url: '/sys/accounts/add',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 登录账号
export function loginaccounts(params) {
	return request({
		url: '/sys/accounts/login',
		method: 'get',
		params,
	});
}

// 更新账号
export function updateaccounts(data) {
	return request({
		url: '/sys/accounts/modify',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 删除账号
export function delaccounts(data) {
	return request({
		url: '/sys/accounts/remove',
		method: 'post',
		data: data,
	});
}

// 账号详情
export function accountsDetail(query) {
	return request({
		url: '/sys/accounts/detail',
		method: 'get',
		params: query,
	});
}

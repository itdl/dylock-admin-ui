/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-18 10:43:16
 * @LastEditors: pj
 * @LastEditTime: 2022-11-21 10:32:30
 */
import request from '@/utils/request';
import store from '@/store';

// 用户列表
export function listcustomers(query) {
	return request({
		url: '/sys/customers/list',
		method: 'get',
		params: { ...query },
	});
}

// 用户分页列表
export function listPagingcustomers(query) {
	return request({
		url: '/sys/customers/listPage',
		method: 'get',
		params: { ...query },
	});
}

// 新增用户
export function addcustomers(data) {
	return request({
		url: '/sys/customers/add',
		method: 'post',
		data,
	});
}

// 更新用户
export function updatecustomers(data) {
	return request({
		url: '/sys/customers/modify',
		method: 'post',
		data,
	});
}

// 删除用户
export function delcustomers(data) {
	return request({
		url: '/sys/customers/remove',
		method: 'post',
		data,
	});
}

// 获取用户钥匙
export function customerskeys(query) {
	return request({
		url: '/sys/customers/keys/list',
		method: 'get',
		params: query,
	});
}

// 删除用户钥匙
export function customersDelkeys(query) {
	return request({
		url: '/sys/customers/keys/remove',
		method: 'get',
		params: query,
	});
}

// 用户详情
export function customersDetail(query) {
	return request({
		url: '/sys/customers/current',
		method: 'get',
		params: query,
	});
}

// 新增钥匙
export function addKeycustomers(data) {
	return request({
		url: '/sys/customers/keys/add',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 设置预警
export function setwarningcustomers(data) {
	return request({
		url: '/sys/customers/setNotOpeningDay',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 入住
export function checkinsustomers(data) {
	return request({
		url: '/sys/checkins/add',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 批量入住
export function checkinsBatchcustomers(data) {
	return request({
		url: '/sys/checkins/batch',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 批量调宿
export function accommodBatchcustomers(data) {
	return request({
		url: '/sys/accommodations/batch',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

export function accommodationsCustomers(data) {
	return request({
		url: '/sys/accommodations/add',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 下发
export function issuedcustomers(data) {
	return request({
		url: '/sys/issueds/add',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

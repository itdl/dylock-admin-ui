/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-21 15:14:57
 * @LastEditors: pj
 * @LastEditTime: 2022-10-21 15:33:02
 */
import request from '@/utils/request';
import store from '@/store';

// 组织机构列表
export function listcsOrgs(params) {
	return request({
		url: '/sys/customers/orgs/tree',
		method: 'get',
		params,
	});
}

// 新增组织机构
export function addcsOrgs(data) {
	return request({
		url: '/sys/customers/orgs/add',
		method: 'post',
		data,
	});
}

// 更新组织机构
export function updatecsOrgs(data) {
	return request({
		url: '/sys/customers/orgs/modify',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 删除组织机构
export function delcsOrgs(params) {
	return request({
		url: '/sys/customers/orgs/remove',
		method: 'get',
		params,
	});
}

// 组织机构详情
// export function buildingsDetail (query) {
//     return request({
//         url: '/sys/buildings/detail',
//         method: 'get',
//         params: query
//     })
// }

import request from '@/utils/request';

// 查询分页角色列表
export function listRole(query) {
	return request({
		url: '/sys/roles/listPage',
		method: 'get',
		params: query,
	});
}

//查询角色列表
export function getRoleList(query) {
	return request({
		url: '/sys/roles/list',
		method: 'get',
		params: query,
	});
}

// 查询角色详细
export function getRole(roleId) {
	return request({
		url: '/sys/roles/detail',
		method: 'get',
		params: {
			roleId: roleId,
		},
	});
}

// 新增角色
export function addRole(data) {
	return request({
		url: '/sys/roles/add',
		method: 'post',
		data: data,
	});
}

// 修改角色
export function updateRole(data) {
	return request({
		url: '/sys/roles/modify',
		method: 'post',
		data: data,
	});
}

// 删除角色
export function delRole(roleId) {
	return request({
		url: '/sys/roles/remove',
		method: 'get',
		params: {
			roleId: roleId,
		},
	});
}

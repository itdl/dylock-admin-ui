import request from '@/utils/request';

// 查询菜单列表
export function listMenu(query) {
	return request({
		url: '/sys/perms/list',
		method: 'get',
		params: query,
	});
}

// 查询菜单详细
export function getMenu(permId) {
	return request({
		url: '/sys/perms/detail',
		method: 'get',
		params: {
			permId: permId,
		},
	});
}

// 查询菜单下拉树结构
export function treeselect() {
	return request({
		url: '/sys/perms/getPermTree',
		method: 'get',
	});
}

// 根据角色ID查询菜单下拉树结构
export function roleMenuTreeselect(roleId) {
	return request({
		url: '/sys/perms/getRolePermTree',
		method: 'get',
		params: {
			roleId: roleId,
		},
	});
}

// 新增菜单
export function addMenu(data) {
	return request({
		url: '/sys/perms/add',
		method: 'post',
		data: data,
	});
}

// 修改菜单
export function updateMenu(data) {
	return request({
		url: '/sys/perms/modify',
		method: 'post',
		data: data,
	});
}

// 删除菜单
export function delMenu(permId) {
	return request({
		url: '/sys/perms/remove',
		method: 'get',
		params: {
			permId: permId,
		},
	});
}

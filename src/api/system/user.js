import request from '@/utils/request';
import { praseStrEmpty } from '@/utils/ruoyi';

// 查询用户列表
export function listUser(query) {
	return request({
		url: '/sys/users/listPage',
		method: 'get',
		params: query,
	});
}
// 查询用户列表
export function listUserNoPage(query) {
	return request({
		url: '/sys/users/list',
		method: 'get',
		params: query,
	});
}

// 查询用户详细
export function getUser(userId) {
	return request({
		url: '/sys/users/detail',
		method: 'get',
		params: {
			userId: userId,
		},
	});
}

// 新增用户
export function addUser(data) {
	return request({
		url: '/sys/users/add',
		method: 'post',
		data: data,
	});
}

// 修改用户
export function updateUser(data) {
	return request({
		url: '/sys/users/modify',
		method: 'post',
		data: data,
	});
}

// 修改密码
export function updatePassword(oldPassword, newPassword) {
	return request({
		url: '/sys/users/modifyPassword',
		method: 'post',
		data: {
			oldPassword,
			newPassword,
		},
	});
}

// 重置密码
export function resetUserPwd(userId, password) {
	return request({
		url: '/sys/users/resetPwd',
		method: 'post',
		data: {
			userId,
			password,
		},
	});
}

// 删除用户
export function delUser(userId) {
	return request({
		url: '/sys/users/remove',
		method: 'get',
		params: {
			userId: userId,
		},
	});
}

// 上传图片
export function uploadFile(data) {
	return request({
		url: '/u/file/upload',
		method: 'post',
		data,
	});
}

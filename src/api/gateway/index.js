/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-15 15:50:56
 * @LastEditors: pj
 * @LastEditTime: 2022-10-19 15:40:32
 */
import request from '@/utils/request';
import store from '@/store';

// 网关列表
export function listgateway(query) {
	return request({
		url: '/sys/gateway/list',
		method: 'get',
		params: { ...query, projectId: store.getters.projectid },
	});
}

// 新增网关
export function addGateway(data) {
	return request({
		url: '/sys/gateway/add',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 更新网关
export function updateGateway(data) {
	return request({
		url: '/sys/gateway/modify',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 删除网关
export function delGateway(data) {
	return request({
		url: '/sys/gateway/remove',
		method: 'post',
		data: data,
	});
}

// 网关详情
export function gatewayDetail(query) {
	return request({
		url: '/sys/gateway/detail',
		method: 'get',
		params: query,
	});
}

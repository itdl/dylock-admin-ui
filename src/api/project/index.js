/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-18 19:04:22
 * @LastEditors: pj
 * @LastEditTime: 2022-10-18 19:08:05
 */
import request from '@/utils/request';

// 项目列表
export function listproject(params = {}) {
	return request({
		url: '/sys/project/list',
		method: 'get',
		showProjectId: false,
		params,
	});
}

// 新增项目
export function addproject(data) {
	return request({
		url: '/sys/project/add',
		method: 'post',
		data: data,
	});
}

// 更新项目
export function updateproject(data) {
	return request({
		url: '/sys/project/modify',
		method: 'post',
		data: data,
	});
}

// 删除项目
export function delproject(data) {
	return request({
		url: '/sys/project/remove',
		method: 'post',
		data: data,
	});
}

// 项目详情
export function projectDetail(query) {
	return request({
		url: '/sys/project/detail',
		method: 'get',
		params: query,
	});
}

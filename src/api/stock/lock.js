import request from '@/utils/request';

// 门锁入库单列表
export function listStocksLk(query) {
	return request({
		url: '/sys/stocks/lock/list',
		method: 'get',
		params: query,
	});
}

// 新增入库入库门锁
export function addStocksLk(data) {
	return request({
		url: '/sys/stocks/lock/add',
		method: 'post',
		data: data,
	});
}

// 更新入库门锁
export function updateStocksLk(data) {
	return request({
		url: '/sys/stocks/lock/modify',
		method: 'post',
		data: data,
	});
}

// 删除入库门锁
export function delStocksLk(data) {
	return request({
		url: '/sys/stocks/lock/remove',
		method: 'post',
		data: data,
	});
}

// 入库门锁详情
export function StocksLkDetail(query) {
	return request({
		url: '/sys/stocks/lock/detail',
		method: 'get',
		params: query,
	});
}

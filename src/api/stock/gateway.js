import request from '@/utils/request';

// 网关入库单列表
export function listStocksGy(query) {
	return request({
		url: '/sys/stocks/gateway/list',
		method: 'get',
		params: query,
	});
}

// 新增网关
export function addStocksGy(data) {
	return request({
		url: '/sys/stocks/gateway/add',
		method: 'post',
		data: data,
	});
}

// 更新网关
export function updateStocksGy(data) {
	return request({
		url: '/sys/stocks/gateway/modify',
		method: 'post',
		data: data,
	});
}

// 删除网关
export function delStocksGy(data) {
	return request({
		url: '/sys/stocks/gateway/remove',
		method: 'post',
		data: data,
	});
}

// 网关详情
export function StocksGyDetail(query) {
	return request({
		url: '/sys/stocks/gateway/detail',
		method: 'get',
		params: query,
	});
}

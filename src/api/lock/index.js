/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-15 15:50:56
 * @LastEditors: pj
 * @LastEditTime: 2022-10-19 16:00:50
 */
import request from '@/utils/request';
import store from '@/store';

// 门锁列表
export function listlock(query) {
	return request({
		url: '/sys/lock/list',
		method: 'get',
		params: { ...query },
	});
}

// 新增门锁
export function addlock(data) {
	return request({
		url: '/sys/lock/add',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 更新门锁
export function updatelock(data) {
	return request({
		url: '/sys/lock/modify',
		method: 'post',
		data: { ...data, projectId: store.getters.projectid },
	});
}

// 删除门锁
export function dellock(data) {
	return request({
		url: '/sys/lock/remove',
		method: 'post',
		data: data,
	});
}

// 门锁详情
export function lockDetail(query) {
	return request({
		url: '/sys/lock/detail',
		method: 'get',
		params: query,
	});
}

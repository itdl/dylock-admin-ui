/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-14 19:24:01
 * @LastEditors: pj
 * @LastEditTime: 2022-10-19 14:46:35
 */
const getters = {
	sidebar: (state) => state.app.sidebar,
	size: (state) => state.app.size,
	device: (state) => state.app.device,
	visitedViews: (state) => state.tagsView.visitedViews,
	cachedViews: (state) => state.tagsView.cachedViews,
	token: (state) => state.user.token,
	avatar: (state) => state.user.avatar,
	name: (state) => state.user.name,
	info: (state) => state.user.info,
	introduction: (state) => state.user.introduction,
	roles: (state) => state.user.roles,
	permissions: (state) => state.user.permissions,
	permission_routes: (state) => state.permission.routes,
	projectid: (state) => state.user.projectid,
};
export default getters;

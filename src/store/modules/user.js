import { login, logout, getInfo } from '@/api/login';
import { getToken, setToken, removeToken } from '@/utils/auth';

const user = {
	state: {
		token: getToken(),
		name: '',
		avatar: '',
		roles: [],
		permissions: [],
		projectid: sessionStorage.getItem('projectId') || '',
		info: {},
	},

	mutations: {
		SET_TOKEN: (state, token) => {
			state.token = token;
		},
		SET_NAME: (state, name) => {
			state.name = name;
		},
		SET_AVATAR: (state, avatar) => {
			state.avatar = avatar;
		},
		SET_ROLES: (state, roles) => {
			state.roles = roles;
		},
		SET_PERMISSIONS: (state, permissions) => {
			state.permissions = permissions;
		},
		SET_PROJECTID: (state, projectid) => {
			sessionStorage.setItem('projectId', projectid);
			state.projectid = projectid;
		},
		SET_INFO: (state, data) => {
			state.info = data;
		},
	},

	actions: {
		changeProjectId({ commit }, proid) {
			commit('SET_PROJECTID', proid);
		},
		// 登录
		Login({ commit }, userInfo) {
			const username = userInfo.username.trim();
			const password = userInfo.password;
			const code = userInfo.code;
			return new Promise((resolve, reject) => {
				login(username, password, code)
					.then((res) => {
						setToken(res.data.accessToken);
						console.log('token:', res.data.accessToken);
						commit('SET_TOKEN', res.data.accessToken);
						resolve();
					})
					.catch((error) => {
						reject(error);
					});
			});
		},

		// 获取用户信息
		GetInfo({ commit, state }) {
			return new Promise((resolve, reject) => {
				getInfo(state.token)
					.then((res) => {
						const user = res.data.user;
						const avatar = user.avatarUrl ? user.avatarUrl : require('@/assets/image/profile.jpg');
						if (res.roles && res.data.roles.length > 0) {
							// 验证返回的roles是否是一个非空数组
							commit('SET_ROLES', res.data.roles);
						} else {
							commit('SET_ROLES', ['ROLE_DEFAULT']);
						}
						commit('SET_PERMISSIONS', res.data.permissions);
						commit('SET_NAME', user.username);
						commit('SET_INFO', user);
						commit('SET_AVATAR', avatar);
						resolve(res);
					})
					.catch((error) => {
						reject(error);
					});
			});
		},

		// 退出系统
		LogOut({ commit, state }) {
			return new Promise((resolve, reject) => {
				// logout(state.token).then(() => {
				commit('SET_TOKEN', '');
				commit('SET_ROLES', []);
				commit('SET_PERMISSIONS', []);
				commit('SET_PROJECTID', '');
				sessionStorage.setItem('projectId', '');
				removeToken();
				resolve();
				// }).catch(error => {
				//   reject(error)
				// })
			});
		},

		// 前端 登出
		FedLogOut({ commit }) {
			return new Promise((resolve) => {
				commit('SET_TOKEN', '');
				commit('SET_PROJECTID', '');
				sessionStorage.setItem('projectId', '');
				removeToken();
				resolve();
			});
		},
	},
};

export default user;

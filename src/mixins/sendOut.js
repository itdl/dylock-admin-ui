import { Message } from 'element-ui';
const WS_FM_Init = 'FM_Init';
const WS_FM_Enroll = 'FM_Enroll';
const WS_FM_Extract = 'FM_Extract';
const WS_FM_GetSdkVerison = 'FM_GetSdkVerison';
const WS_FM_DetectFinger = 'FM_DetectFinger';
const WS_FM_GetDevVerison = 'FM_GetDevVerison';
const WS_FM_Deinit = 'FM_Deinit';
const WS_RET_ERR_OK = '0'; //操作成功
const WS_RET_ERR_PARAM = '-1'; //参数错误
const WS_RET_ERR_MEMORY = '-2'; //内存分配失败，没有分配到足够的内存
const WS_RET_ERR_FUN = '-3'; //功能未实现
const WS_RET_ERR_DEVICE = '-4'; //设备不存在
const WS_RET_ERR_INIT = '-5'; //设备未初始化
const WS_RET_ERR_UNKOWN = '-6'; //非法错误号
const WS_RET_ERR_EXTRACT = '-7'; //提取特征失败或合成模板失败
const WS_RET_ERR_ELSE = '-9'; //其它错误
const WS_FPM_FeatureMatch = '';
const textObk = {
	'-1': '参数错误',
	'-2': '内存分配失败，没有分配到足够的内存',
	'-3': '功能未实现',
	'-4': '设备不存在',
	'-5': '设备未初始化',
	'-6': '非法错误号',
	'-7': '提取特征失败或合成模板失败',
	'-9': 'WS_RET_ERR_ELSE',
};
const errAlert = (iret) => {
	if (iret === WS_RET_ERR_OK) {
		Message.success('操作成功');
		return true;
	} else if (iret === WS_RET_ERR_PARAM) {
		Message.error('参数错误');
		return false;
	} else if (iret === WS_RET_ERR_MEMORY) {
		Message.error('内存分配失败，没有分配到足够的内存');
	} else if (iret === WS_RET_ERR_FUN) {
		Message.error('功能未实现');
		return false;
	} else if (iret === WS_RET_ERR_DEVICE) {
		Message.error('设备不存在');
		return false;
	} else if (iret === WS_RET_ERR_EXTRACT) {
		Message.error('提取特征失败或合成模板失败');
		return false;
	} else if (iret === WS_RET_ERR_INIT) {
		Message.error('设备未初始化');
		return false;
	} else if (iret === WS_RET_ERR_UNKOWN) {
		Message.error('非法错误号');
		return false;
	} else if (iret === WS_RET_ERR_ELSE) {
		Message.error('其它错误');
		return false;
	}
};
const sendOut = {
	data() {
		return {
			fingerprint: 0,
			id: null,
			text: '已成功链接设备，已检测到手指',
			ws: null,
		};
	},
	methods: {
		connect() {
			this.fingerprint = 0;
			if (this.ws != null) {
				this.msgSuccess('已连接到设备');
				this.text = '已成功链接设备，已检测到手指';
				return;
			}
			const url = 'ws://localhost:9618';
			if ('WebSocket' in window) {
				this.ws = new WebSocket(url);
			} else if ('MozWebSocket' in window) {
				this.ws = new MozWebSocket(url);
			} else {
				this.msgError('unsupported WebSocket');
				this.text = 'unsupported WebSocket';
				return;
			}
			this.ws.onopen = () => {
				this.msgSuccess('open');
				this.ws.binaryType = 'arraybuffer';
			};
			this.ws.onmessage = (e) => {
				const s = e.data.toString();
				const obj = JSON.parse(s);
				if (obj.repcode === WS_FM_Init) {
					if (obj.result !== '0') {
						this.text = textObk[obj.result];
						errAlert(obj.result);
					}
				} else if (obj.repcode === WS_FM_Deinit) {
					//
					errAlert(obj.result);
				} else if (obj.repcode === WS_FM_GetDevVerison) {
				} else if (obj.repcode === WS_FM_GetSdkVerison) {
					this.msgSuccess(obj.version);
				} else if (obj.repcode === WS_FM_DetectFinger) {
					if (obj.result === '1') {
						this.text = '已成功连接设备,已检测到手指' + this.fingerprint;
					} else {
						this.text = textObk[obj.result];
						errAlert(obj.result);
					}
				} else if (obj.repcode === WS_FM_Extract) {
					if (obj.result === WS_RET_ERR_OK) {
						this.msgSuccess('录入指纹成功');
					} else {
						this.text = textObk[obj.result];
						errAlert(obj.result);
					}
				} else if (obj.repcode === WS_FM_Enroll) {
					if (obj.result === WS_RET_ERR_OK) {
						this.id = obj.template;
						this.text = '合成模版成功';
						this.msgSuccess('合成模版成功');
					} else {
						errAlert(obj.result);
					}
				} else if (obj.repcode === WS_FPM_FeatureMatch) {
					errAlert(obj.result);
				}
			};
			this.ws.onclose = () => {
				this.msgError('closed');
				this.text = '设备不存在';
				this.ws = null;
			};
			this.ws.onerror = () => {
				this.msgError('error');
				this.text = '设备不存在';
				this.fingerprint = 0;
				this.ws = null;
			};
		},
		send(s) {
			if (this.ws != null) {
				this.ws.send(s);
				return true;
			}
			this.fingerprint = 0;
			this.msgError('暂无设备');
			return false;
		},
		FM_Init() {
			this.text = '已成功连接设备';
			this.fingerprint = 0;
			const obj = { reqcode: WS_FM_Init };
			const str = JSON.stringify(obj);
			return this.send(str);
		},
		FM_Enroll() {
			this.fingerprint += 1;
			const obj = {
				reqcode: WS_FM_Extract,
				index: this.fingerprint,
			};
			const str = JSON.stringify(obj);
			console.log(this.fingerprint, 'fingerprint');
			this.send(str);
		},
		FM_DetectFinger_OnClick() {
			const obj = { reqcode: WS_FM_DetectFinger };
			const str = JSON.stringify(obj);
			this.send(str);
		},
		FM_Enroll_OnClick() {
			const obj = { reqcode: WS_FM_Enroll };
			const str = JSON.stringify(obj);
			this.send(str);
		},
	},
};

export default sendOut;

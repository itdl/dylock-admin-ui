const table = {
	data() {
		return {
			showSearch: true,
			tableAction: 0,
		};
	},
	methods: {
		updateTableActionWidth() {
			this.tableAction = 100;
			this.$nextTick(() => {
				const action = document.querySelectorAll('.table-action');
				const button = [];
				for (let i = 0; i < action.length; i++) {
					button.push(action[i].querySelectorAll('.el-button'));
				}

				const width = Array.from(Array(button.length).keys()).map(() => 0);
				for (let i = 0; i < button.length; i++) {
					for (let j = 0; j < button[i].length; j++) {
						width[i] += button[i][j].clientWidth || 0;
					}
				}
				this.tableAction = this.tableAction + Math.max.apply(null, width);
				console.log(this.tableAction, 'ta');
			});
		},
		downloadFile(api, fileName) {
			this.$request
				.get(api, {
					params: this.queryParams,
					responseType: 'blob',
				})
				.then((res) => {
					console.log(res, 'res');
					this.downFile(res, fileName);
				});
		},
	},
};
export default table;

import Quill from 'quill';
var fonts = ['Microsoft-YaHei', 'SimSun', 'SimHei', 'KaiTi', 'Arial', 'Times-New-Roman'];
var Font = Quill.import('formats/font');
Font.whitelist = fonts; // 将字体加入到白名单

const fontSizeStyle = Quill.import('attributors/style/size');
fontSizeStyle.whitelist = ['12px', '14px', '16px', '20px', '24px', '36px'];
Quill.register(fontSizeStyle, true);
// 工具栏配置
const toolbarOptions = [
	['bold', 'italic', 'underline', 'strike'], // 加粗 斜体 下划线 删除线
	['blockquote', 'code-block'], // 引用  代码块
	[{ header: 1 }, { header: 2 }], // 1、2 级标题
	[{ list: 'ordered' }, { list: 'bullet' }], // 有序、无序列表
	[{ script: 'sub' }, { script: 'super' }], // 上标/下标
	[{ indent: '-1' }, { indent: '+1' }], // 缩进
	// [{'direction': 'rtl'}],                         // 文本方向
	[{ size: fontSizeStyle.whitelist }], // 字体大小
	[{ header: [1, 2, 3, 4, 5, 6, false] }], // 标题
	[{ color: [] }, { background: [] }], // 字体颜色、字体背景颜色
	[{ font: fonts }], // 字体种类
	[{ align: [] }], // 对齐方式
	['clean'], // 清除文本格式
	['image'], // 链接、图片、视频, 'video'
];
import { uploadSingleFile } from '@/api/icrm/repository';
export default {
	data() {
		return {
			editorOption: {
				theme: 'snow', // or 'bubble'
				placeholder: '您想说点什么？',
				modules: {
					toolbar: {
						container: toolbarOptions,
						// container: "#toolbar",
						handlers: {
							image: function (value) {
								if (value) {
									// 触发input框选择图片文件
									document.querySelector('.avatar-uploader input').click();
								} else {
									this.quill.format('image', false);
								}
							},
						},
					},
				},
			},
			serverUrl: '/v1/blog/imgUpload', // 这里写你要上传的图片服务器地址
			header: {
				// token: sessionStorage.token
			},
		};
	},
	methods: {
		onEditorChange() {
			// 内容改变事件
			this.$emit('input', this.content);
		},
		uploadFile(params) {
			const types = params.file.type;
			console.log('uploadFile', params);
			let type = '';
			if (types.indexOf('image') !== -1) {
				type = 'image';
			} else if (types.indexOf('video') !== -1) {
				type = 'video';
				const isLt100M = params.file.size / 1024 / 1024 < 100;
				if (!isLt100M) {
					this.msgError('上传视频不能超过100M');
					return;
				}
			} else {
				this.msgError('请上传图片或者是视频');
				return;
			}
			//待 调试上传
			console.log('上传-----------');
			console.log(params);
			let formData = new FormData();
			formData.append('file', params.file);
			console.log('formData--', formData);
			uploadSingleFile(formData)
				.then((res) => {
					console.log(`上传成功回调`);
					const quill = this.$refs.myQuillEditor.quill;
					const length = quill.getSelection().index;
					quill.insertEmbed(length, type, res.data);
					quill.setSelection(length + 1);
				})
				.catch((err) => {
					console.log(`阿里云OSS上传图片失败回调`, err);
				});
			// this.$ossClient
			//   .uploadFile(params.file)
			//   .then(url => {
			//     console.log(`上传成功回调`)
			//     const quill = this.$refs.myQuillEditor.quill
			//     const length = quill.getSelection().index
			//     quill.insertEmbed(length, type, url)
			//     quill.setSelection(length + 1)
			//   })
			//   .catch(err => {
			//     console.log(`阿里云OSS上传图片失败回调`, err)
			//   })
		},
	},
};

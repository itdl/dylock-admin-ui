/*
 * @Descripttion:
 * @Version: 1.0
 * @Author: pj
 * @Date: 2022-10-14 19:24:01
 * @LastEditors: pj
 * @LastEditTime: 2022-11-22 14:16:57
 */
import Vue from 'vue';

import Cookies from 'js-cookie';

import 'normalize.css/normalize.css'; // a modern alternative to CSS resets
import 'windi.css';
import Element from 'element-ui';
import './assets/styles/element-variables.scss';

import '@/assets/styles/index.scss'; // global css
import '@/style/common.css';
import '@/assets/styles/ruoyi.scss'; // ruoyi css
import App from './App';
import store from './store';
import router from './router';
import permission from './directive/permission';
import './assets/icons'; // icon
import './permission'; // permission control
import { parseTime, resetForm, addDateRange, selectDictLabel, selectDictLabels, download, handleTree } from '@/utils/ruoyi';
import { downFile } from '@/utils';
import Pagination from '@/components/Pagination';
import StartEndTime from '@/components/StartEndTime';
import Upload from '@/components/Upload';
import Organization from '@/components/Organization';
import OrganizationSelect from '@/components/Organization/select';
import Source from '@/components/Source';
import SourceSelect from '@/components/Source/select';
import ImportFile from '@/components/Import';
import request from './utils/request';
// 富文本编辑
import VueQuillEditor from 'vue-quill-editor';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';
Vue.use(VueQuillEditor);
//自定义表格工具扩展
import RightToolbar from '@/components/RightToolbar';
import RightToolbarRadio from '@/components/RightToolbar/radio';
import ExportList from '@/components/ExportList';
// 全局方法挂载
Vue.prototype.parseTime = parseTime;
Vue.prototype.resetForm = resetForm;
Vue.prototype.selectDictLabel = selectDictLabel;
Vue.prototype.selectDictLabels = selectDictLabels;
Vue.prototype.addDateRange = addDateRange;
Vue.prototype.download = download;
Vue.prototype.handleTree = handleTree;
Vue.prototype.downFile = downFile;
Vue.prototype.msgSuccess = function (msg) {
	this.$message({ showClose: true, message: msg, type: 'success' });
};

Vue.prototype.msgError = function (msg) {
	this.$message({ showClose: true, message: msg, type: 'error' });
};

Vue.prototype.msgInfo = function (msg) {
	this.$message.info(msg);
};

Vue.prototype.emptyToString = (row, column, cellValue, index) => {
	if (cellValue) {
		return cellValue;
	} else {
		return '-';
	}
};
Vue.prototype.emptyToNumber = (row, column, cellValue, index) => {
	if (cellValue) {
		return cellValue;
	} else {
		return 0;
	}
};

Vue.prototype.$request = request;

// 全局组件挂载
Vue.component('Pagination', Pagination);
Vue.component('RightToolbar', RightToolbar);
Vue.component('rightToolbar-radio', RightToolbarRadio);

Vue.component('ExportList', ExportList);
Vue.component('StartEndTime', StartEndTime);
Vue.component('l-upload', Upload);
Vue.component('l-organization', Organization);
Vue.component('l-organization-select', OrganizationSelect);
Vue.component('l-source', Source);
Vue.component('l-source-select', SourceSelect);
Vue.component('l-import', ImportFile);
Vue.use(permission);

import scroll from 'vue-seamless-scroll';
import 'windi.css';
Vue.use(scroll);
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

Vue.use(Element, {
	size: Cookies.get('size') || 'medium', // set element-ui default size
});

Vue.config.productionTip = false;

new Vue({
	el: '#app',
	router,
	store,
	render: (h) => h(App),
});
